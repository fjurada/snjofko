<?php

use \Phalcon\Tag;

class IndexController extends BaseController
{
	
	public function onConstruct()
	{
		parent::initialize();
	}

	public function indexAction()
	{
		Tag::setTitle('Početna');
		if ($this->session->get('logged_in') == 1) {
			$this->response->redirect('userpanel/index');
		}		
	}

	// test samo za generiranje password hasha
	public function hashPassAction($pass)
	{
		echo $this->security->hash($pass);
	}

	public function signoutAction()
	{
		$this->session->destroy();
		$this->response->redirect('index/');
	}

	public function aboutAction()
	{
		Tag::setTitle("O nama");
		
		echo var_dump();
		echo($this->session->get('role'));
	}
	
		
	public function userAction($usermail)
	{
		if($usermail == NULL)
		{
			echo "No user queried. You queried: ".$usermail;
			$this->response->redirect('index/');
			return;
		}
		
		$user = User::findFirst('email = "'.$usermail.'"');

		if($user == NULL)
		{
			echo "User ".$usermail." Non existant";
			$this->response->redirect('index/');
			return;
		}
		
		echo $user->id;
		
		$ads = Ad::findByUser_id($user->id);	
		$users = User::find();
		
		$this->view->setVar('user',$user);
		$this->view->setVar('ads',$ads);
		$this->view->setVar('users',$users);
	}
}
