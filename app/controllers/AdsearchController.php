<?php 

use \Phalcon\Tag;

class AdsearchController extends BaseController
{

	public function onConstruct()
	{
		parent::initialize();
	}

	public function indexAction()
	{
		
		Tag::setTitle('Pretraga');
		
		$query = $this->request->getQuery('q');
		
		$allads = Ad::find();
		
		$ads= array();
		
		foreach($allads as $ad)
		{
			if (strcmp($ad->title, $q) or strcmp($ad->description, $q)) 
			{
				array_push($ads, $ad);
			}			
		}
		
		$length = sizeof($ads);
		
		$this->view->setVar('length',$length);
		$this->view->setVar('ads',$ads);
	}

}

